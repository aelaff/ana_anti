var Observable = require('FuseJS/Observable');
var Storage = require("FuseJS/Storage");

 
module.exports = {
	getUserData:function(){
		var data = Storage.readSync("AnaWAnti_user_data.js");
 		if(data == null || data == "" )
			return null;
		else{
			return JSON.parse(data);
		  }
	},
	
	setUserData:function(data){
		Storage.writeSync("AnaWAnti_user_data.js",JSON.stringify(data));
	},
	logout:function(data){
		Storage.writeSync("AnaWAnti_user_data.js",null);
	}
};