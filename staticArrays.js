var Observable = require("FuseJS/Observable");
var Session = require("Sessions.js");

var text_Message = Observable("");
var registrationAims=Observable();
var countries=Observable();
var nationalities=Observable();
var socialStatuses=Observable();
var haveSons=Observable();
var educationalQualifications=Observable();
var occupations=Observable();
var communicationWays=Observable();
var eyeColors=Observable();
var skinColors=Observable();
var lengthCategories=Observable();
var weightCategories=Observable();
var ages=Observable();
var hairColors=Observable();
var last_seen=Observable();
var has_khatabat=Observable("true");
var has_messages=Observable("true");
var has_likes=Observable("true");

var subscriptions=Observable();
var subscriptions_filtered=Observable();
var subscription_id= Observable("");
subscription_id.value= Session.getUserData() == null ? null : Session.getUserData().profile[0].subscription_id;


subscriptions.forEach(function(s) {
	    if (s.id == subscription_id.value) {
	       subscriptions_filtered.clear();
	       subscriptions_filtered.add(s);
	    }
	});

module.exports={
	registrationAims,
	ages,
	countries,
	nationalities,
	socialStatuses,
	haveSons,
	educationalQualifications,
	occupations,
	communicationWays,
	eyeColors,
	skinColors,
	lengthCategories,
	weightCategories,
	hairColors,
	last_seen,
	subscriptions,
	subscriptions_filtered,
	has_khatabat,
	 has_messages,
	 has_likes
}
