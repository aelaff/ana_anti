var Observable = require("FuseJS/Observable");
var Camera = require("FuseJS/Camera");
var CameraRoll = require("FuseJS/CameraRoll");
var ImageTools = require("FuseJS/ImageTools");
var ImageCompressor = require("ImageCompressor");

var exports = module.exports;

//  These observables will be used to display an image and its information

var compressedImage = exports.compressedImage = Observable();
var imagePath = exports.imagePath = Observable();
var imageName = exports.imageName = Observable();
var imageSize = exports.imageSize = Observable();

//  This is used to keep the last image displayed as a base64 string in memory
var lastImage = "";
//  When we receive an image object we want to display, we call this
var displayImage = function(image)
{

  // {"path":"","width":320,"height":240,"info":{}}
      // console.log("hhh : "+JSON.stringify(image));
  imagePath.value = image.path;
  ImageTools.getImageFromBase64(image).then(
    function(b64)
    {
      lastImage = b64;
      console.log(lastImage)

    }
  );
}      
/*
    1. Take an unscaled "raw" picture
    2. Pass the picture into ImageTools.resize to scale and then crop it to 320x320
    3. Publish the scaled image to the device cameraroll
    4. Display the final image
*/

exports.takePicture = function()
{
  Camera.takePicture(1200,628).then(
    function(image) {
        console.log("Original Image : "+JSON.stringify(image));
            imagePath.value= image.path;

        ImageCompressor.CompressImagePromise(image.path).then(function(data){
          //console.log("Compressed Image : "+JSON.stringify(data));
          //console.log("data.path : "+data.path);
          try{
            CameraRoll.publishImage(data);
            compressedImage.value= data.path;
          }
          catch(err){
            console.log("err: " + err);
          }

          //displayImage(data);
        });
    }
  ).catch(
    function(reason){
      console.log("Couldn't take picture: " + reason);
    }
  );
};

/*
  1. Take an unscaled "raw" picture
  2. Crop the image with a rectangle and save the result to a new file.
  3. Display the new image.
*/

exports.takeCroppedPicture = function()
{
  Camera.takePicture().then(
    function(image)
    {
      var options = { x: 20, y:20, width:320, height:320 };
      ImageTools.crop(image, options).then(
        function(image)
        {
          displayImage(image);
        }
      );
    }
  );
}

/*
  1. Take an aspect-corrected 100x100 resized image
  2. Get the image bytes as an arraybuffer
  3. Pass the image bytes back in to create a new image from them
  4. Display the returned image
*/

exports.takeSmallPicture = function()
{
  Camera.takePicture(100, 100).then(
    function(image) {
      ImageTools.getBufferFromImage(image).then(
        function(buffer) {
          ImageTools.getImageFromBuffer(buffer).then(
            function(image) {
              displayImage(image);
            }
          )
        }
      )
    }
  ).catch(
    function(reason){
      console.log("Couldn't take picture: "+reason);
    }
  );
}

/*
  1. Spawn a dialog to fetch an image from the camera roll
  2. Display the image
*/

exports.selectImage = function()
{
  CameraRoll.getImage().then(
     function(image) {
      var options = {
          mode: ImageTools.SCALE_AND_CROP ,
          desiredWidth: 512, //The desired width in pixels
          desiredHeight: 512  //The desired height in pixels
      };
      //console.log("Original Image : "+JSON.stringify(image));
          imagePath.value= image.path;
          ImageCompressor.CompressImagePromise(image.path).then(function(data){
          //console.log("Compressed Image : "+JSON.stringify(data));
          //console.log("data.path : "+data.path);
          try{
            // CameraRoll.publishImage(data);
            compressedImage.value= data.path;
          }
          catch(err){
            console.log("err: " + err);
          }
          // displayImage(data);
      });
    }
  ).catch(
    function(reason){
      console.log("Couldn't get image: "+reason);
    }
  );
};

/*
  Bounce the last displayed image via base64 and display the reloaded image
*/
exports.b64Test = function()
{
  ImageTools.getImageFromBase64(lastImage).then(
    function(image){
      displayImage(image);
      console.log(image);
    }
  );
};