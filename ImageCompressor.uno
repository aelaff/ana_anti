using Uno;
using Uno.UX;
using Uno.Collections;
using Fuse;
using Fuse.Scripting;

using Uno.Compiler.ExportTargetInterop;


/*[extern(iOS) Require("Xcode.FrameworkDirectory", "@('FacebookSDKs-iOS':Path)")]
[extern(iOS) Require("Xcode.Framework", "@('FacebookSDKs-iOS/FBSDKCoreKit.framework':Path)")]
[extern(iOS) Require("Xcode.Framework", "@('FacebookSDKs-iOS/FBSDKLoginKit.framework':Path)")]
[extern(iOS) ForeignInclude(Language.ObjC, "FBSDKCoreKit/FBSDKCoreKit.h")]
[extern(iOS) ForeignInclude(Language.ObjC, "FBSDKLoginKit/FBSDKLoginKit.h")]*/

[Require("Gradle.Dependency","compile('id.zelory:compressor:2.1.0')")]
[ForeignInclude(Language.Java, "android.os.*")]
[ForeignInclude(Language.Java, "java.io.*")]
[ForeignInclude(Language.Java, "id.zelory.compressor.*")]
[ForeignInclude(Language.Java, "android.graphics.Bitmap")]
[ForeignInclude(Language.Java, "android.util.Log")]
[UXGlobalModule]

public class ImageCompressor : NativeModule
{
	public ImageCompressor()
	{
		Resource.SetGlobalKey(this, "ImageCompressor");
		AddMember( new NativeFunction("CompressImage", (NativeCallback)CompressImage) );
        AddMember(new NativePromise<string, Fuse.Scripting.Object>("CompressImagePromise", CompressImagePromise, Converter));
	}

	object CompressImage(Context c, object[] args)
	{
		// CompressImage will return:
		// from iOS: [language designator]-[script designator]-[region designator] (e.g. zh-Hans-US, en-US, etc.)
		// from Android: [two-leter lowercase language code (ISO 639-1)]_[two-letter uppercase country codes (ISO 3166-1)] (e.g. zh_CN, en_US, etc.)
		var path  = (string)args[0];
		return doCompressImage(path);
	}
    static string CompressImagePromise(object[] args)
    {
        if (args.Length != 1)
        {
            // This will reject the promise with the message of the exception
            throw new Exception("CompressImagePromise() requires exactly 1 parameter.");
        }

        // This will resolve the promise
		var path  = (string)args[0];
		return doCompressImage(path);
    }

	static Fuse.Scripting.Object Converter(Context context, string str)
    {
        var wrapperObject = context.NewObject();
        wrapperObject["path"] = str;
        return wrapperObject;
    }

	[Foreign(Language.Java)]
	static extern(Android) string doCompressImage(string imagePath)
	@{
        try {
            File f =  new File(imagePath);
            
            String randomText = System.currentTimeMillis()+".jpg";
             File compressedImageFile = new 
             Compressor(com.fuse.Activity.getRootActivity())
             .setQuality(75)
             .setMaxWidth(1260)
          	 .setMaxHeight(708)
             .setCompressFormat(Bitmap.CompressFormat.JPEG)
             .setDestinationDirectoryPath(com.fuse.Activity.getRootActivity().getExternalCacheDir().getAbsolutePath()+"/images")
            .compressToFile(f,randomText);
    
			return compressedImageFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
			return "ERROR";
        }
	@}

	[Foreign(Language.ObjC)]
	static extern(iOS) string doCompressImage(string imagePath)
	@{
		return imagePath;
	@}

	static extern(!(iOS||Android)) string doCompressImage(string imagePath)
	{
		// return the preferred language for unimplemented platforms
		return "Default";
	}
}