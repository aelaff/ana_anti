var ApiUrl = "http://huraa.co/api/";
function apiFetch(path, options) {
    var url = encodeURI(ApiUrl+path);
    if(options === undefined) {
        options = {};
    }

    if(options.body !== undefined) {
        options = Object.assign({}, options, {
            body: JSON.stringify(options.body),
            headers: Object.assign({}, options.headers, {
                "Content-Type": "application/json"
            })
        });
    }

    return fetch(url, options)
        .then(function(response) {
            return response.json();
        });
}
function apiFetchForm(path, options) {
    var url = encodeURI(ApiUrl+path);
            console.log("url: "+ url);
    if(options === undefined) {
        options = {};
    }
    var posting_data = "";
    for ( var key in options.body ) {
        posting_data = posting_data + key +  "="+options.body[key] + "&";
    }
    posting_data = posting_data.substring(0, posting_data.length-1);
            console.log("posting_data: "+ posting_data);
    if(options.body !== undefined) {
        options = Object.assign({}, options, {
            body: posting_data,
            headers: Object.assign({}, options.headers, {
                "Content-Type": "application/x-www-form-urlencoded"
            })
        });
    }

    return fetch(url, options)
        .then(function(response) {
            return response.json();
        });
}
module.exports={
    apiFetch:apiFetch,
    apiFetchForm
}